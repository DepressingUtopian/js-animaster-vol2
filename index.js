(function main() {
    document.getElementById("fadeInPlay").addEventListener("click", function () {
        const block = document.getElementById("fadeInBlock");
        animaster().addFadeIn(1000).play(block);
    });

    document.getElementById("scalePlay").addEventListener("click", function () {
        const block = document.getElementById("scaleBlock");
        let customAnimation = animaster().addScale(1000, 1.25);
        customAnimation.play(block);
    });

    document.getElementById("movePlay").addEventListener("click", function () {
        const block = document.getElementById("moveBlock");
        animaster().move(block, 1000, { x: 100, y: 10 });
    });

    const customMove = animaster()
        .addMove(1000, { x: 0, y: -100 })
        .addMove(1000, { x: 800, y: -100 })
        .addMoveAndScaleCustom(2000, 2)
        .buildHandler(false);

    document.getElementById("customMoveBlock")
        .addEventListener("click", customMove);

    document.getElementById("scalePlay").addEventListener("click", function () {
        const block = document.getElementById("scaleBlock");
        let customAnimation = animaster().addScale(1000, 1.25);
        customAnimation.play(block);
        animaster().scale(block, 1000, 1.25);
    });

    document
        .getElementById("fadeOutPlay")
        .addEventListener("click", function () {
            const block = document.getElementById("fadeOutBlock");
            animaster().addFadeOut(1000).play(block);
        });

    let moveAndHideStopper;
    document
        .getElementById("moveAndHidePlay")
        .addEventListener("click", function () {
            const block = document.getElementById("moveAndHideBlock");
            moveAndHideStopper = animaster()
                .moveAndHide(5000)
                .play(block, false);
        });

    document
        .getElementById("moveAndHideReset")
        .addEventListener("click", function () {
            moveAndHideStopper.reset();
        });

    document
        .getElementById("showAndHidePlay")
        .addEventListener("click", function () {
            const block = document.getElementById("showAndHideBlock");
            animaster().showAndHide(5000).play(block, false);
        });

    let heartBeatingTimerId;
    document
        .getElementById("heartBeatingPlay")
        .addEventListener("click", function () {
            const block = document.getElementById("heartBeatingBlock");
            heartBeatingTimerId = animaster().heartBeating().play(block, true);
        });

    document
        .getElementById("heartBeatingStop")
        .addEventListener("click", function () {
            if (heartBeatingTimerId !== undefined) {
                heartBeatingTimerId.stop();
                heartBeatingTimerId = undefined;
            }
        });

    let shakingTimerId;
    document
        .getElementById("shakingPlay")
        .addEventListener("click", function () {
            const block = document.getElementById("shakingBlock");
            shakingTimerId = animaster().shaking().play(block, true);
        });

    document
        .getElementById("shakingStop")
        .addEventListener("click", function () {
            if (shakingTimerId !== undefined) {
                shakingTimerId.stop();
                shakingTimerId = undefined;
            }
        });
    let playHandler;
    document.getElementById("testPlay").addEventListener("click", function () {

        const block = document.getElementById("testBlock");
        playHandler = animaster()
            .addMove(2000, { x: 100, y: 10 })
            .addMove(4000, { x: 400, y: 10 })
            .play(block, true);
    });
    document.getElementById("testStop").addEventListener("click", function () {
        if (playHandler !== undefined) {
            playHandler.stop();
        }
    });
    document.getElementById("resetPlay").addEventListener("click", function () {
        if (playHandler !== undefined) {
            playHandler.reset();
        }
    });
    //CUSTOM ANIMATION
    function getRandomInt(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min)) + min; //Максимум не включается, минимум включается
    }
    function genStar(starType, heightContainer, widthContainer) {
        let star;
        if (starType === 'bigStar')
            star = document.querySelector('.custom-container .big-star').cloneNode(true);
        else if (starType == 'littleStar')
            star = document.querySelector('.custom-container .little-star').cloneNode(true);
        else if (starType == 'middleStar')
            star = document.querySelector('.custom-container .middle-star').cloneNode(true);
        else
            return null;


        return star;
    }
    let playHandlerCustomArray = [];
    let starObjects = [];
    document.getElementById("customPlay").addEventListener("click", function () {
        const customContainer = document.querySelector('.custom-container');
        const heightContainer = customContainer.offsetHeight;
        const widthContainer = customContainer.offsetWidth;
        const starTypes = ['bigStar', 'littleStar', 'middleStar'];
        const starCount = 100;

        starObjects.forEach(elem => {
            elem.remove();
        });
        starObjects = [];
        for (let i = 0; i < starCount; i++) {
            const typeStarId = getRandomInt(0, 3);
            const newStar = genStar(starTypes[typeStarId], heightContainer, widthContainer);
            customContainer.appendChild(newStar);
            const beginPosX = widthContainer - getRandomInt(0, widthContainer);
            const beginPosY = heightContainer - getRandomInt(0, heightContainer);
            newStar.style.left = beginPosX + 'px';
            newStar.style.top = beginPosY + 'px';
            const randomDelay = getRandomInt(500, 40000);

            const offsetPosX = widthContainer - newStar.offsetWidth - beginPosX - (widthContainer - beginPosX);
            const offsetPosY = heightContainer - newStar.offsetHeight - beginPosY;

            let playHandler = animaster()
                .addFadeIn(100)
                .addMove(randomDelay, { x: offsetPosX, y: offsetPosY })
                .addFadeOut(100)
                .play(newStar, true);
            playHandlerCustomArray.push(playHandler);
            starObjects.push(newStar);

        }
    });
    document.getElementById("customStop").addEventListener("click", function () {
        if (playHandlerCustomArray !== undefined) {
            playHandlerCustomArray.forEach(playHandler => {
                playHandler.stop();
            });
        }
    });
    document.getElementById("customResetPlay").addEventListener("click", function () {
        if (playHandlerCustomArray !== undefined) {
            playHandlerCustomArray.forEach(playHandler => {
                playHandler.reset();
                starObjects.forEach(elem => {
                    elem.remove();
                });
            });
        }
    });
})();

function getTransform(translation, ratio) {
    const result = [];
    if (translation) {
        result.push(`translate(${translation.x}px,${translation.y}px)`);
    }
    if (ratio) {
        result.push(`scale(${ratio})`);
    }
    return result.join(" ");
}
function getRotate(degree) {
    const result = [];
    if (degree) {
        result.push(`rotate(${degree}deg)`);
    }
    return result;
}

function animaster() {

    // /**
    //  * Функция имитирующая сердцебиение (одну итерацию)
    //  * @param {HTMLElement} — HTMLElement, с которым нужно взаимодействовать
    //  */
    /**
     * Функция встряхивающая элемент (одна итерацию)
     * @param {HTMLElement}  — HTMLElement, который надо встрясти
     */
    function shakingStep(element) {
        animaster().move(element, 250, { x: 20, y: 0 });
        setTimeout(() => {
            animaster().move(element, 250, { x: 0, y: 0 });
        }, 250);
    }

    let animasterObject = {
        /**
         * Функция заставляет элемент плавно появляться
         * @param element  — HTMLElement, с которым нужно взаимодействовать
         * @param duration — длитеьльность анимации в милисекундах
         */

        fadeIn: function (element, duration) {
            element.style.transitionDuration = `${duration}ms`;
            element.classList.remove("hide");
            element.classList.add("show");
        },

        /**
         * Функция, сбрасывает анимацию FadeIn
         * @param element — HTMLElement, который надо анимировать
         */
        resetFadeIn: function (element) {
            element.classList.remove("show");
            element.classList.add("hide");
            element.style.transitionDuration = null;
        },

        /**
         * Функция заставляет элемент плавно исчезать
         * @param element — HTMLElement, который надо анимировать
         * @param duration — Продолжительность анимации в миллисекундах
         * @param translation — Объект с полями x и y, обозначающими смещение блока
         */
        fadeOut: function (element, duration) {
            element.style.transitionDuration = `${duration}ms`;
            element.classList.remove("show");
            element.classList.add("hide");
        },

        /**
         * Функция, сбрасывает анимацию fadeOut
         * @param element — HTMLElement, который надо анимировать
         */
        resetFadeOut: function (element) {
            element.style.transitionDuration = null;
            element.classList.remove("hide");
            element.classList.add("add");
        },

        /**
         * Функция, передвигающая элемент
         * @param element — HTMLElement, который надо анимировать
         * @param duration — Продолжительность анимации в миллисекундах
         * @param translation — объект с полями x и y, обозначающими смещение блока
         */
        move: function (element, duration, translation) {
            this.addMove(duration, translation).play(element);
            return this;
        },

        /**
         * Функция, увеличивающая/уменьшающая элемент
         * @param element — HTMLElement, который надо анимировать
         * @param duration — Продолжительность анимации в миллисекундах
         * @param ratio — во сколько раз увеличить/уменьшить. Чтобы уменьшить, нужно передать значение меньше 1
         */
        scale: function (element, duration, ratio) {
            this.addScale(duration, ratio).play(element);
            return this;
        },

        /**
         * Функция сбрасывает изменение положения и размера
         * @param element — HTMLElement, который надо анимировать
         */
        resetMoveAndScale: function (element) {
            element.style.transitionDuration = null;
            element.style.transform = null;
        },

        /**
         * Функция сначала сдвигает элемент потом плавно прячет
         * @param element — HTMLElement, который надо анимировать
         * @param duration — общая длительность анимации
         */
        moveAndHide: function (duration) {
            this.addMove((duration * 2) / 5, { x: 100, y: 20 })
                .addDelay((duration * 2) / 5)
                .addFadeOut((duration * 3) / 5);

            return this;
        },
        /**
         * Функция сначала показывает элемент, затем плавно размывает
         * @param element — HTMLElement, который надо анимировать
         * @param duration — общая длительность анимации
         */
        showAndHide: function (duration) {
            this.addFadeIn(duration / 3)
                .addDelay((duration * 2) / 3)
                .addFadeOut(duration / 3);
            return this;
        },
        addDelay: function (duration) {
            let delayAction = (duration) => {
                setTimeout(() => {

                }, duration);
            };
            this.addStep(delayAction, duration);
            return this;
        },
        /**
         * Функция бесконечно  имитирующая сердцебиение
         * @param {HTMLElement} — HTMLElement, с которым нужно взаимодействовать
         */
        heartBeating: function () {
            this.addScale(500, 1.4).addScale(500, 1);
            return this;
        },
        /**
         * Функция бесконечно встряхивающая элемент
         * @param {HTMLElement} — HTMLElement, который надо встрясти
         */
        shaking: function () {
            this.addMove(250, { x: 20, y: 0 }).addMove(250, { x: 0, y: 0 })
            return this;
        },

        addStep(linkFunc, duration, params) {
            this._steps.push({ linkFunc: linkFunc, duration, params });
        },

        addMove: function (duration, translation) {
            let moveAction = (element, duration, params) => {
                element.style.transitionDuration = `${duration}ms`;
                element.style.transform = getTransform(params.translation, null);
            };
            this.addStep(moveAction, duration, { translation: translation });
            return this;
        },

        addMoveAndScaleCustom: function (duration, ratio) {
            let moveAndScaleAction = (element, duration, ratio) => {
                element.style.transitionDuration = `${duration}ms`;
                element.style.transform = `scale(${ratio}) translate(700px, -100px)`;
            };
            this.addStep(moveAndScaleAction, duration, null, ratio);
            return this;
        },

        addScale: function (duration, ratio) {
            let scaleAction = (element, duration, params) => {
                element.style.transitionDuration = `${duration}ms`;
                element.style.transform = getTransform(null, params.ratio);
            };
            this.addStep(scaleAction, duration, { ratio: ratio });
            return this;
        },
        addFadeIn: function (duration) {
            let fadeInAction = (element, duration) => {
                element.style.transitionDuration = `${duration}ms`;
                element.classList.remove("hide");
                element.classList.add("show");
            };
            this.addStep(fadeInAction, duration);
            return this;
        },

        addFadeOut: function (duration) {
            let fadeOutAction = (element, duration) => {
                element.style.transitionDuration = `${duration}ms`;
                element.classList.remove("show");
                element.classList.add("hide");
            };
            this.addStep(fadeOutAction, duration);
            return this;
        },
        ///----CUSTOM ANIMATIONS----///
        addRotate: function (duration, degree) {
            let rotateAction = (element, duration, params) => {
                element.style.transitionDuration = `${duration}ms`;
                element.style.transform = getRotate(params.degree);
            }
            this.addStep(rotateAction, duration, { degree: degree });
            return this;
        },
        addRotateAndMoveAndScale: function (duration, degree, translation, ratio) {
            let rotateAction = (element, duration, params) => {
                element.style.transitionDuration = `${duration}ms`;
                if (params.degree)
                    element.style.transform = getRotate(params.degree);
                if (params.translation)
                    element.style.transform += " " + getTransform(params.translation, null);
                if (params.ratio)
                    element.style.transform += " " + getTransform(null, params.ratio);
            }
            this.addStep(rotateAction, duration, { degree: degree, translation: translation, ratio: ratio });
            return this;
        },

        _steps: [],

        play: function (element, cycled) {
            let stopFlag = false;
            let cycledAnimationTimeout;
            let stepsTimeouts = [];
            let computeSumStepsDuration = () => {
                let sumStepsDuration = 0;
                this._steps.forEach((step) => {
                    sumStepsDuration += step.duration;
                });
                return sumStepsDuration;
            }
            let stopEvent = () => {
                stopFlag = true;
                if (cycledAnimationTimeout) {
                    clearInterval(cycledAnimationTimeout);
                }
                stepsTimeouts.forEach((timeoutId) => {
                    clearTimeout(timeoutId);
                });
                stepsTimeouts = [];
            };
            let resetElement = () => {
                this.resetFadeIn(element);
                this.resetFadeOut(element);
                this.resetMoveAndScale(element);
            };

            let animationStep = () => {
                let durationMemory = 0;
                this._steps.forEach((step) => {
                    if (stopFlag) {
                        return;
                    }
                    let currentTimeoutId = setTimeout(
                        step.linkFunc.bind(
                            this,
                            element,
                            step.duration,
                            step.params
                        ),
                        durationMemory
                    );
                    stepsTimeouts.push(currentTimeoutId);
                    durationMemory += step.duration;
                });
            };

            animationStep();
            if (cycled) {
                stepsTimeouts = [];
                resetElement();
                cycledAnimationTimeout = setInterval(() => {
                    animationStep();
                    stepsTimeouts = [];
                    resetElement();
                }, computeSumStepsDuration());
            }

            return {
                stop: () => stopEvent(),
                reset: () => {
                    stopEvent();
                    resetElement();
                },
            };
        },

        buildHandler: function (cycled) {
            let that = this;
            return function () {
                that.play(this, cycled);
            };
        },
    };

    return animasterObject;
}
